#
# koala CMS
#
# Author: Marco Somma

from django import forms

from core.models.article import Article
from core.models.resource import Resource
from core.models.logo import Logo


class ArticleForm(forms.ModelForm):
    text = forms.CharField( widget=forms.Textarea )
    class Meta:
        model = Article

class ResourceForm(forms.ModelForm):
    class Meta:
        model = Resource

class LogoForm(forms.ModelForm):
    class Meta:
        model = Logo