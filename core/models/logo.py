#
# koala CMS
#
# Author: Marco Somma
from django.db import models

STATUS_CHOICES = (
    (1, 'Journals'),
    (2, 'Companies'),
    (3, 'Others'),
)


def get_folder(instance, filename):
    return "logos/%s/%s"%(instance.type,filename)

class Logo(models.Model):
    name = models.CharField(max_length=125)
    type = models.IntegerField(choices=STATUS_CHOICES, default=1)
    file  = models.FileField(upload_to=get_folder)
    date = models.DateTimeField(auto_now=True)


    def __unicode__(self):
        return self.name

    class Meta:
        app_label = 'core'
        db_table = 'logos'
        verbose_name = 'logo'
        verbose_name_plural = 'logos'
        ordering = ['type']