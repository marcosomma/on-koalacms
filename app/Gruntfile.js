module.exports = function(grunt) {

  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-requirejs');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-copy');

  var userConfig = require( './build.config.js' );

  var taskConfig = {
    pkg: grunt.file.readJSON('package.json'),

	jshint: {
	  // define the files to lint
	  files: ['gruntfile.js', 'js/**/*.js'],
	  // configure JSHint (documented at http://www.jshint.com/docs/)
	  options: {
	      // more options here if you want to override JSHint defaults
	    globals: {
	      jQuery: true,
	      console: true,
	      module: true
	    }
	  }
	},
	clean: {
		build: [
	      '<%= dist_dir %>/templates', 
	      '<%= dist_dir %>/*.txt',
		],
		prod: {
	      src: '<%= static_dir %>/js',
	      options: {
	    	force: true,
	      }
		},
    },
	requirejs: {
		compile: {
			options: {
			    appDir: '<%= app_dir %>',
			    baseUrl: '<%= base_url %>',
			    dir: '<%= dist_dir %>',
			    modules: [
			        {
			            name: 'main'
			        }
			    ],
			    fileExclusionRegExp: /^((r|build|Gruntfile|build.config)\.js)|.json|.git|.gitignore|.styl|Thumbs.db|node_modules$/,
			    optimizeCss: 'standard',
			    removeCombined: true,
			    paths: {
			        jquery: 'libs/jquery',
			        underscore: 'libs/underscore',
			        backbone: 'libs/backbone',
			        jstree: 'libs/jstree',
			        text: 'libs/require/text',
			        templates: '../templates'
			    },
			    shim: {
			        underscore: {
			            exports: '_'
			        },
			        jstree: {
			            exports: 'jstree'
			        },
			        backbone: {
			            deps: [
			                'underscore',
			                'jquery'
			            ],
			            exports: 'Backbone'
			        }
			    }
			}
		}
	},
	copy: {
	  main: {
	    cwd: '<%= dist_dir %>/',
	    expand: true,
	    src: '**',
	    dest: '<%= static_dir %>/',
	  },
	  dev: {
	    expand: true,
	    src: ['<%= app_dir %>/js/**', '<%= app_dir %>/templates/**'],
	    dest: '<%= static_dir %>',
	  }
	},
  };

  grunt.initConfig( grunt.util._.extend( taskConfig, userConfig ) );

  grunt.registerTask('hints', ['jshint']);
  grunt.registerTask('build', ['requirejs', 'clean:build']);
  grunt.registerTask('dev', ['clean:prod', 'copy:dev']);
  grunt.registerTask('prod', ['build', 'clean:prod', 'copy:main']);
};