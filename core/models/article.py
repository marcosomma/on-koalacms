#
# koala CMS
#
# Author: Marco Somma
from django.db import models
from logo import Logo


class Article(models.Model):
    title = models.CharField(max_length=125)
    subtitle = models.CharField(max_length=125)
    text = models.CharField(max_length=2000)
    logo = models.ForeignKey(Logo, on_delete=models.SET_NULL, blank=True, null=True)
    date = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return self.title

    class Meta:
        app_label = 'core'
        db_table = 'articles'
        verbose_name = 'article'
        verbose_name_plural = 'articles'
        ordering = ['date']