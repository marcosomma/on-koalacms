import unittest

from django.test import Client
from django.core import management

# Create your tests here.

class ResourceModelTestCase(unittest.TestCase):

    def setUpClass(self):
        self.c = Client()
        management.call_command('loaddata', 'core/tests/fixtures/resource.json', verbosity=0)