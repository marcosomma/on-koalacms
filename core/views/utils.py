def get_article_resources(article):
    desired_format = '%d/%m/%Y'
    #desired_format = '%d/%m/%Y - %H:%M'
    logo = { 'id': article.logo.id, 'title': article.logo.name, 'url': article.logo.file.url , 'date': article.logo.date.strftime(desired_format)} if article.logo else []
    context = {'id':article.id,
               'title': article.title,
               'subtitle': article.subtitle,
               'text': article.text,
               'logo': logo,
               'date':article.date.strftime(desired_format)}
    return context