#
# koala CMS
#
# Author: Marco Somma

from django import template
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render_to_response, get_object_or_404
from django.template.context import RequestContext
from django.contrib.auth.decorators import login_required

from core.models.logo import Logo
from core.forms import LogoForm


@login_required
def manage_logos(request, logo=None):

    if request.method == 'POST':

        form = LogoForm(request.POST, request.FILES)

        if form.is_valid():
            instance = form.save(commit=False)
            instance.save()
            return HttpResponse(instance.name)
        else:
            print "no valid"
            print form.errors
    else:

        if logo is None:
            form = LogoForm()
        else:
            form = LogoForm(instance = logo)

    data = {'form': form}

    context = RequestContext(request, data)

    return render_to_response('article_form.html', context_instance=context)