import unittest

from django.test import Client
from django.core import management

# Create your tests here.

class ArticleModelTestCase(unittest.TestCase):

    def setUpClass(self):
        self.c = Client()
        management.call_command('loaddata', 'core/tests/fixtures/articles.json', verbosity=0)
