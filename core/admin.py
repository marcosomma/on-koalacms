from django.contrib import admin

from core.models.article import Article
from core.models.resource import Resource
from core.models.logo import Logo

admin.site.register(Resource)
admin.site.register(Article)
admin.site.register(Logo)