import unittest
import logging

from django.test import Client
from django.core import management

from auth.models import MyUser

# Create your tests here.

class MyAuthModelTestCase(unittest.TestCase):

    @classmethod
    def setUpClass(self):
        self.c = Client()
        management.call_command('loaddata', 'core/tests/fixtures/user.json', verbosity=0)
        self.super_user = MyUser.objects.get(email = 'm@superuser.com')
        self.user = MyUser.objects.get(email = 'm@user.com')

    def test_auth_logging(self):
        self.c.logout()
        self.assertEqual(self.c.login(email=self.super_user.email, password='1234'), True)
        self.c.logout()
        self.assertEqual(self.c.login(email=self.user.email, password='1234'), True)
        logging.info( 'test_auth_logging OK')

    def test_auth_get_full_name(self):
        self.assertEqual(self.super_user.get_full_name(), '%s %s' % (self.super_user.first_name, self.super_user.last_name))
        self.assertEqual(self.user.get_full_name(), '%s %s' % (self.user.first_name, self.user.last_name))
        logging.info( 'test_auth_get_full_name OK')


    def test_auth_get_short_name(self):
        self.assertEqual(self.super_user.get_short_name(), '%s' %self.super_user.first_name)
        self.assertEqual(self.user.get_short_name(), '%s' %self.user.first_name)
        logging.info( 'test_auth_get_short_name OK')

    def test_auth_get_all_data(self):
        superuser = self.super_user.get_all_data()
        user = self.user.get_all_data()
        self.assertEqual(superuser['email'], self.super_user.email)
        self.assertEqual(user['email'], self.user.email)
        self.assertEqual(superuser['first_name'], self.super_user.first_name)
        self.assertEqual(user['first_name'], self.user.first_name)
        self.assertEqual(superuser['last_name'], self.super_user.last_name)
        self.assertEqual(user['last_name'], self.user.last_name)
        self.assertEqual(superuser['id'], self.super_user.id)
        self.assertEqual(user['id'], self.user.id)
        self.assertEqual(superuser['date_joined'], self.super_user.date_joined)
        self.assertEqual(user['date_joined'], self.user.date_joined)
        logging.info( 'test_auth_get_all_data OK')