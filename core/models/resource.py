#
# koala CMS
#
# Author: Marco Somma
from article import Article

from django.db import models

def get_folder(instance, filename):
    return "press/%s/%s"%(instance.article.identifier,filename)

class Resource(models.Model):
    title = models.CharField(max_length=50)
    file  = models.FileField(upload_to=get_folder)
    date = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return self.title

    class Meta:
        app_label = 'core'
        db_table = 'resources'
        verbose_name = 'resource'
        verbose_name_plural = 'resources'
        ordering = ['date']