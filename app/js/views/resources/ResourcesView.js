define([
    'jquery',
    'underscore',
    'backbone',
    'helpers/ajax',
    'helpers/popup',
    'text!templates/resources/template.html'
], function( $, _, Backbone, Ajax, Popup, Template ) {

    var ResourcesView = Backbone.View.extend({

        initialize: function() {

            this.upload_index = 0;
            this.upload_references = [];
            this.article_id = this.options.extras.article_id;
            this.fileselect = $('input[type="file"]');

            if (window.File && window.FileList && window.FileReader) {
                this.file_init();
            }

            $('#filedrag').on('click', _.bind(this.onTargetClick, this));
            $('.btn-delete, .btn-preview').on('click', this.onDelete);

            Popup.register(this.on_dropdown);

            this.render();
        },

        on_dropdown: function(id, action) {
            console.log(id, action);
        },

        file_init: function() {
            var fileselect = $('#fileselect')[0],
                filedrag = $('#filedrag')[0];

            this.fileselect.on("change", _.bind(this.fileSelectHandler, this));

            var xhr = new XMLHttpRequest();
            if (xhr.upload) {
    
                // file drop
                filedrag.addEventListener("dragover", this.fileDragHover, false);
                filedrag.addEventListener("dragleave", this.fileDragHover, false);
                filedrag.addEventListener("drop", _.bind(this.fileSelectHandler, this), false);
            }
        },

        render: function() {
            /*var t = _.template( Template, { } );
            this.$el.html(t);*/

        },

        create_element: function( type, classname ) {
            return $("<" + type + ">").addClass(classname);
        },

        fileDragHover: function(e) {
            e.stopPropagation();
            e.preventDefault();

            var target = $(e.currentTarget);
            if (e.type === "dragover") {
                target.addClass("hover");
            } else {
                target.removeClass("hover");                
            }
        },

        fileSelectHandler: function(e) {
            var files, i;
            this.fileDragHover(e);

            files = e.target.files || e.dataTransfer.files;

            for (i = 0; f = files[i]; i+=1) {
                this.addFile(f);
                this.uploadFile(f);
            }
        },

        addFile: function(file) {
            var li = this.get_li(file.name);

            this.upload_references.push(li);
            this.$el.prepend(li);
        },

        uploadFile: function(file) {
            var formData = new FormData();
            formData.append('file', file);
            formData.append('title', file.name);

            Ajax.send('/resources/' + this.article_id, formData, this.upload_index, _.bind(this.onProgress, this), _.bind(this.onSuccess, this), _.bind(this.onError, this));

            this.upload_index += 1;
        },

        get_li: function(name) {
            var li = this.create_element('li');
            var name = this.create_element('span').text(name);
            var timestamp = this.create_element('span', 'timestamp').text('Uploading...');
            var progressbar = this.create_element('div', 'progressbar');
            var progress = this.create_element('span', 'progress');

            progressbar.append(progress);

            li.append(name);
            li.append(timestamp);
            li.append(progressbar);

            return li;
        },

        get_actions_button: function() {
            var btn = this.create_element('button', 'actionbar-button button dropdown-btn');
            var span = this.create_element('span', 'dropdown_arrow');

            btn.attr('data-panel', 'actions-panel');
            btn.text('Actions');
            btn.append(span);

            return btn;
        },

        onTargetClick: function(e) {
            this.fileselect.trigger('click');
        },

        onProgress: function(index, percent) {
            this.upload_references[index].find('.progress').css('width', percent + '%');
        },

        onSuccess: function(index, e) {
            var li = this.upload_references[index];
            var new_span = this.create_element('span', 'new').text('new');
            var img = this.create_element('img' , 'preview');
            var actions_btn = this.get_actions_button();

            img.attr('src', '/media/press/'+ $("span.selected").text() + '/' + e);

            li.find('.timestamp').remove();
            li.find('.progressbar').remove();

            li.append(img)
                .append(new_span)
                .append(actions_btn);

            Popup.register(this.on_dropdown);
        },

        onError: function(index) {
            var li = this.upload_references[index];
            var new_span = this.create_element('span', 'error').text('error');

            li.find('.timestamp').remove();
            li.find('.progressbar').remove();
            li.append(new_span);
        },

        onDelete: function(e) {
            e.preventDefault();
        }

    });

    return ResourcesView;
});