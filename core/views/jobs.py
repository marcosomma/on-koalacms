#
# koala CMS
#
# Author: Marco Somma

from django import template
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render_to_response, get_object_or_404
from django.template.context import RequestContext
from django.contrib.auth.decorators import login_required

from core.models.resource import Resource

@login_required
def landing(request):
    data = {'resources': Resource.objects.order_by('id')}
    context = template.RequestContext(request, data)
    template_name = 'jobs.html'
    #return render_to_response(template_name, context_instance=RequestContext(request, context))
    return render_to_response(template_name, context_instance=RequestContext(request, {}))

