#
# koala CMS
#
# Author: Marco Somma

from django import template
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render_to_response, get_object_or_404
from django.template.context import RequestContext
from django.contrib.auth.decorators import login_required

from core.models.article import Article
from core.models.resource import Resource
from core.forms import ResourceForm, ArticleForm

@login_required
def landing(request):
    data = {'resources': Resource.objects.order_by('id')}
    context = template.RequestContext(request, data)
    template_name = 'resources.html'
    return render_to_response(template_name, context_instance=RequestContext(request, context))

@login_required
def manage_resources(request, article_id):

    article = get_object_or_404(Article, id=article_id)

    if request.method == 'POST':

        form = ResourceForm(request.POST, request.FILES)

        if form.is_valid():
            instance = form.save(commit=False)
            instance.article = article
            instance.save()
            return HttpResponse(instance.title)
        else:
            print "no valid"
            print form.errors
    else:
        form_resource = ResourceForm()
        form_article = ArticleForm()

    resources = Resource.objects.filter(article=article_id)
    data = {
        'form_resource': form_resource,
        'form_article': form_article,
        'resources': resources,
        'article': article
    }
    context = template.RequestContext(request, data)
    template_name = 'resources.html'

    return render_to_response(template_name, context_instance=RequestContext(request, context))
