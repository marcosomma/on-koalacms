#
# koala CMS
#
# Author: Marco Somma

from django import template
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render_to_response, get_object_or_404
from django.template.context import RequestContext

@login_required
def home(request):
    context = template.RequestContext(request, {})
    template_name = 'home.html'
    return render_to_response(template_name, context_instance=RequestContext(request, context))

def sign_in(request):
    state = ""
    username = password = ''
    if request.POST:
        username = request.POST.get('email')
        password = request.POST.get('password')

        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                return HttpResponseRedirect('/')
            else:
                state = "Your account is not active, please contact the site admin."
        else:
            state = "The username or password you entered is incorrect."

    return render_to_response('login.html', context_instance=RequestContext(request, { 'username': username}))

def sign_out(request):
    logout(request)
    return HttpResponseRedirect('/login')