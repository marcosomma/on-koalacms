/**
 * This file/module contains all configuration for the build process.
 */
module.exports = {

  app_dir: './',
  base_url: './js',
  dist_dir: 'dist',
  static_dir: '../core/static',

};