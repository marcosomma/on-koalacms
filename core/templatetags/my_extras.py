from django import template
from django.template.defaultfilters import stringfilter

register = template.Library()

@register.filter
@stringfilter
def mytimesince(value):
    return value.split(',')[0]
mytimesince.is_safe = True