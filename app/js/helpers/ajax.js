define([
    'jquery',
    'backbone',
    'utils/cookies',
], function( $, Backbone, Cookies ) {

	return {

        csrfSafeMethod: function(method) {
            // these HTTP methods do not require CSRF protection
            return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
        },

        config: function() {
            var that = this;
            var csrftoken = Cookies.getCookie('csrftoken');

            $.ajaxSetup({
                crossDomain: false, // obviates need for sameOrigin test

                //Ajax events
                beforeSend: function(xhr, settings) {
                    if (!that.csrfSafeMethod(settings.type)) {
                        xhr.setRequestHeader("X-CSRFToken", csrftoken);
                    }
                },
                //Options to tell jQuery not to process data or worry about content-type.
                cache: false,
                contentType: false,
                processData: false
            });
        },

        send: function(url, data, index, progress_callback, success_callback, error_callback) {

            if (this.initialized === undefined) {
                this.config();
            }

            $.ajax({
                type: "POST",
                url: url,
                async: true,
                data: data,

                xhr: function() {
                    var myXhr = $.ajaxSettings.xhr();
                    if(myXhr.upload){
                        myXhr.upload.addEventListener('progress', this.myprogress, false); // For handling the progress of the upload
                    }
                    return myXhr;
                },

                myprogress: function(e) {
                    var done = e.position || e.loaded, total = e.totalSize || e.total;
                    var present = Math.floor(done/total*100);
                    if (progress_callback) progress_callback.call(this, index, present);
                },
                
                success: function(e) {
                    if (success_callback) success_callback.call(this, index, e);
                },
                error: function(){
                    if (error_callback) error_callback.call(this, index);
                }
            });
        }

	}
});
