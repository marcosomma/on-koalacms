define([
    'jquery',
    'underscore',
], function( $, _ ) {

    var instance = null,
        btn_instance = null,
        callback = null;

    return {

        toggle: function() {

            this.clickHandler = this.clickHandler || _.bind(this.onDocumentClick, this);

            instance.toggleClass('active');

            if (instance.hasClass('active')) {
                this.set_position_panel();
                $(document).on('click', this.clickHandler);
            } else {
                $(document).off('click', this.clickHandler);
                instance = null;
            }
        },

        register: function(callback) {
            var that = this;

            this.callback = callback;

            $('.dropdown-btn').off('click');
            $('.dropdown-panel .option').off('click');

            $('.dropdown-btn').on('click', function(e) {
                var current_id;
                e.preventDefault();
                e.stopPropagation();

                if (instance) {
                    current_id = $(e.target).attr('data-panel');
                    if (instance.attr('id') !== current_id) {
                        that.toggle();
                    }
                }

                btn_instance = $(e.currentTarget);

                var panel_id = btn_instance.attr('data-panel');

                instance = $('#' + panel_id);
                that.toggle();
            });

            $('.dropdown-panel .option').on('click', function(e) {
                that.toggle();
                var resid = btn_instance.attr('data-resid');
                var action = $(e.currentTarget).attr('id');

                callback.call(this, resid, action);
            });
        },

        set_position_panel: function() {
            var pos = btn_instance.offset();
            var hdiff = Math.abs(btn_instance.outerWidth() - instance.outerWidth());

            instance.css({
                'left': pos.left - hdiff,
                'top': pos.top + btn_instance.outerHeight()
            });
        },

        onDocumentClick: function(e) {
            var inPanel = $(e.target).parents('.action-menu').length !== 0;
            if (!inPanel) this.toggle();
        }
	}
});
