#
# koala CMS
#
# Author: Marco Somma

from django.conf.urls import patterns, include, url
from django.conf import settings

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    
    url(r'^$', 'core.views.common.home', name='home'),
    url(r'^articles/$', 'core.views.articles.landing', name='articles'),
    url(r'^jobs/$', 'core.views.jobs.landing', name='jobs'),
    url(r'^resources/$', 'core.views.resources.landing', name='resources'),

    url(r'^create/article/$', 'core.views.articles.create_article', name='create_article'),
    url(r'^update/article/(?P<article_id>.+)$', 'core.views.articles.update_article', name='update_article'),
    url(r'^delete/article/(?P<article_id>.+)$', 'core.views.articles.delete_article', name='delete_article'),
    url(r'^create/logo/$', 'core.views.logos.manage_logos', name='manage_logos'),
    url(r'^get-article/(?P<article_id>.+)$', 'core.views.articles.get_article'),
    url(r'^get-articles/$', 'core.views.articles.get_article'),
    url(r'^login/$', 'core.views.common.sign_in'),
    url(r'^logout/$', 'core.views.common.sign_out', name='logout'),

    # url(r'^blog/', include('blog.urls')),

    url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {
       'document_root': settings.MEDIA_ROOT,
    }),
    url(r'^admin/', include(admin.site.urls)),
)
