define([
    'jquery',
    'underscore',
    'backbone',
    'views/LogoView',
], function( $, _, Backbone, LogosView ) {
    
    var AppRouter = Backbone.Router.extend({

        routes: {
            '*actions': 'default_action' // Default
        },

        init: function() {
            this.on_ready();
        },

        show_view: function(viewClass, model, extras) {
            var args = { model: model||this.model, extras: extras };
            var params = _.extend({ el: '#upload-list' }, args);

            this.current_view = new viewClass(params);
        },

        show_logos: function() {
            app_router.show_view(LogosView, { }, { article_id: this.article_id });
        },

        clean_view: function() {

            if (this.current_view) {
                this.current_view.close();
                this.current_view.$el.html('');
            }
        },

        show_404: function() {
            console.log("content not found!");
        },

        default_action: function() {
            this.clean_view();
            var url = window.location.pathname;
            if (url.indexOf('update/article') > -1 ) {
                this.show_logos();
            }
            if (url.indexOf('create/article') > -1 ) {
                this.show_logos();
            }
        },

        on_ready: function() {

            if (app_router.callback) {
                app_router.callback();
                app_router.callback = undefined;
            }
        }
    });

    var initialize = function(){

        app_router = new AppRouter;
    };

    return { 
        initialize: initialize
    };
});