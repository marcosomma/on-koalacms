#
# koala CMS
#
# Author: Marco Somma
from django import template
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render_to_response, get_object_or_404
from django.template.context import RequestContext
from django.contrib.auth.decorators import login_required

from core.models.article import Article
from core.forms import ArticleForm, LogoForm
from core.views.utils import get_article_resources

import json

@login_required
def landing(request):
    data = { 'articles': Article.objects.order_by('id')}
    context = template.RequestContext(request, data)
    template_name = 'articles.html'
    return render_to_response(template_name, context_instance=RequestContext(request, context))

def create_or_update_article(request, article=None):

    is_update = article != None

    if request.method == 'POST':
        if is_update:
            form = ArticleForm(request.POST, instance=article)
        else:
            form = ArticleForm(request.POST)

        if form.is_valid():
            new_instance = form.save()

            return HttpResponseRedirect('/articles')
        else:
            print "no valid"
            print form.errors
    else:
        if article is None:
            form = ArticleForm()
        else:
            form = ArticleForm(instance = article)
    form_logo = LogoForm()
    data = {
        'form': form,
        'form_logo': form_logo,
        'is_update': is_update }

    context = RequestContext(request, data)

    return render_to_response('article_form.html', context_instance=context)

@login_required
def create_article(request):
    return create_or_update_article(request)

@login_required
def update_article(request, article_id):
    article = get_object_or_404(Article, id=article_id)

    return create_or_update_article(request, article)

@login_required
def delete_article(request, article_id):
    article = get_object_or_404(Article, id=article_id)
    article.delete()

    return HttpResponseRedirect('/articles')

@login_required
def get_article(request, article_id=None):
    if article_id:
        article = get_object_or_404(Article, id=article_id)
        context = get_article_resources(article)

    else:
        context = []
        articles = Article.objects.all()
        for article in articles:
            context.append(get_article_resources(article))

    response = HttpResponse(json.dumps(context, sort_keys=True), content_type="application/json")
    response['Access-Control-Allow-Origin'] = '*'

    return response