define([
    'jquery',
    'underscore',
    'backbone',
    'helpers/ajax',
    'helpers/popup',
    'text!templates/logos/template.html'
], function( $, _, Backbone, Ajax, Popup, Template ) {

    var ResourcesView = Backbone.View.extend({

        initialize: function() {

            this.div_log_form = $('#form_logo');

            $('#add_logo').on('click', _.bind(this.onAddClick, this));
            $('#form_logo_add_btn').on('click', _.bind(this.uploadFile, this));
            $('#form_logo_cancel_btn').on('click', _.bind(this.onCancelClick, this));
            this.render();
        },

        render: function() {},

        create_element: function( type, classname ) {
            return $("<" + type + ">").addClass(classname);
        },

        uploadFile: function(e) {
            e.preventDefault();
            var formData = new FormData(),
                url = window.location.origin + '/create/logo/';
            formData.append('file', document.getElementById('id_file').files[0]);
            formData.append('name', $('#form_logo').find('input[id="id_name"]').val());
            formData.append('type', $('#form_logo').find('select[id="id_type"]').val());

            Ajax.send(url, formData, this.upload_index, _.bind(this.onProgress, this), _.bind(this.onSuccess, this), _.bind(this.onError, this));
        },

        onAddClick: function(e) {
            e.preventDefault();
            this.div_log_form.css('display', 'block');
        },

        onCancelClick: function(e) {
            e.preventDefault();
            if(this.div_log_form.css('display')=='block'){
                this.div_log_form.css('display', 'none');
            }
        },

        onProgress: function(index, percent) {
           console.log(percent)
        },

        onSuccess: function(index, e) {
            location.reload();
        },

        onError: function(index,e) {
            console.log(e);

            var div = $('#form_logo');
            var new_span = this.create_element('span', 'error').text('error '+ e);

            div.append(new_span);
        }

    });

    return ResourcesView;
});